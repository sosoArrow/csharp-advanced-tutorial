﻿namespace CommunityToolkitDemoFramework.Services
{
    public interface IDemoService
    {
        string GetInfo();
    }
}